# Computer Aided Design (CAD)

This week, the objective of the module is to get used with the 3D modelisation. The tool used is OpenSCAD. 

## 1. Getting *OpenSCAD*

Download openscad [here](https://openscad.org/) and install it by following indicated steps that can depend on your Operating System (Linux, Windows or Mac).

## 2. Get used with *OpenSCAD*

Several tutorials exists on the internet. Personally, I first followed the [following tutorial](https://gitlab.com/fablab-ulb/enseignements/fabzero/cad/-/blob/main/OpenSCAD.md) by Nicolas DECOSTER in order to discover the syntax and the features of OpenSCAD. Then, since I am quite used with programming languages, I directly helped myself from the [Cheat Sheet](https://openscad.org/cheatsheet/) that resumes the functions that can be used in OpenSCAD to code a box and a nut (see following sections). Finally, to have a practical example of a flexible piece, I read [this post](https://fablab-ulb.gitlab.io/enseignements/2021-2022/fabzero-experiments/students/pauline.mackelbert/FabZero-Modules/module02/) written by Pauline Mackelberg.  

Note the following good practices in OpenSCAD, and in coding in general:
- Try to parameterized as most as possible the piece you are designing.
- Use indentation to keep your code readable.
- Use comments to describe your variables and parts of your code.
- To avoid code repetition, you can use functions with the syntax `module fct_name(){...}` in OpenSCAD.  
- Don't forget to license your work. 

### 2.1. Designing a box

An attempt to design a simple box has been done in OpenSCAD. The result is shown in the figure below. It calls the Minkowski sum in 3 dimensions (more theorical informations about that are available [here](https://en.wikipedia.org/wiki/Minkowski_addition)). The code used to generated this piece is also presented below. There is here only one parameter that is `side` that controls the size of the box. The final dimensions are intrinsically included in the code and are computed relatively to the value given to `side`.

![](images/mod2/mod2_screen_box001.png "Box with Minkowski")

```
// File : mod2_minko.scad
// Author : Morgan Tonglet
// Date : 26 February 2023
// License : Creative Commons Attribution-ShareAlike 4.0 International [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)

//Global parameter
$fn=50; //face number (e.g. 50 to have high curve resolution)

//Piece generation
minkowski(){
    side=10;
    sphere(side/4);
    cube([side*2,side,side/4]);
}
```

### 2.2. Designing a nut

This subsection is about designing a nut. Parameter are `fn` and `radius` which respectively stand for the number of faces of the nut and for the radius of it. It also uses a function/module which allows to easily reuse the part of the code to design several nuts for instance. The result is shown in the image below. 

![](images/mod2/mod2_screen_nut001.png "Box with Minkowski")

```
// File : mod2_ecrou.scad
// Author : Morgan Tonglet
// Date : 26 February 2023
// License : Creative Commons Attribution-ShareAlike 4.0 International [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)

//Constant
e=0.01;

//Parameter
fn=8;
radius=8;

//Functions
module ecrou(extR, intR, h, fn){
    difference(){
        cylinder(h=h,r=extR,center=true, $fn=fn);
        cylinder(h=h+e,r=intR,center=true, $fn=fn);
    }
}

//Render the piece
ecrou(radius, radius/2, radius/2, fn);

```

### 2.3. Designing a flexilink

The objective of this section is to design a flexilink, which is a 3D flexible component that can be used with LEGO bricks in order to compose a compliant mechanism. More informations and some examples are available [here](https://www.compliantmechanisms.byu.edu/flexlinks). The example that is developed in this section is the one named _Fixed-Fixed Beam - Initially curved_ and available on this previous website. Note excepted the idea of the piece, the code developed in this section is an original creation.


#### Basic part

The first part to design is the one that comes onto the LEGO brick and shown in the picture below. To generate the outline of the piece, two cylinders were initially positioned and a convex hull was created between them. Then, three holes were created by making the difference between the first convex hull and three cylinders for holes. The number of holes is a parameter named `n_holes` in the code. The total length of the piece (for the convex hull) has also been parameterized according to the number of holes. On top of it, a `gap` parameter has been added in order to be able to add space between holes. The image below shows how this parameter influences the global shape of the piece. 

![](images/mod2/mod2_screens_fl001.png "")

Note that there is also two parameters that condition the internal and the external radii of the cylinders, respectively the `rad_in` and `rad_ext` parameters. All of it is included in the code below. This part was coded in a `module ..(..){..}` so it can easily be reused later.


```
// Author : Morgan Tonglet
// Date : 26 February 2023
// License : Creative Commons Attribution-ShareAlike 4.0 International [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)

$fn=64;

h=5;
rad_in=4;
rad_ext=5;
rad_arc=50;
n_holes=3;
holes_gap=0;
thick=1;

module part(n_holes,h,r_in,r_ext,gap){
  difference(){
    //drawing the external part
    hull(){
        cylinder(h,r=r_ext);
        translate([0,(n_holes-1)*(2*r_ext+gap),0])
        cylinder(h,r=r_ext);
    };
    //removing material for holes
    union(){
      for(i=[0:n_holes-1]){
        translate([0,i*(2*r_ext+gap),0])
        cylinder(h,r=r_in);
      }
    }
  }
}

translate([0,rad_ext,0])//not rad_in
part(n_holes,h,rad_in,rad_ext,holes_gap);

```

#### Arc part

The previous code allows us to build easily two basic part. However, there is still a flexible part that should be designed in order to link those part. This is the goal of this subsection.  

The link built is a 90° arc circle. It is made of the difference between two cylinders, the smallest being soustracted to the biggest. A parameter `thick` has been introduced to be able to easily control the thickness of the circular arc as it can be seen in the code below. The other main parameter is `rad_arc` that control the radius of this circular arc. `h` stands for the height of the piece, and the other parameters are the same as described above in the _Basic part_ section. As before, everything is contained in a module. The figure below shows how the piece looks.

![](images/mod2/mod2_screen_arc.png "")


```
// Author : Morgan Tonglet
// Date : 26 February 2023
// License : Creative Commons Attribution-ShareAlike 4.0 International [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)

$fn=64;
e=0.05;

h=5;
rad_in=4;
rad_ext=5;
rad_arc=50;
thick=1;

module arc(thickness, h, radius){
  t=thickness;
  d=(rad_ext-rad_in)/2; //parameter inserted to avoid bugs for the parts combination (details below)
  difference(){
    cylinder(h=h,r=radius+t/2); 
    //biggest cylinder
      translate([0,0,-e]) 
      //avoid display bugs when previewing in OpenSCAD
      union(){
        //smallest cylinder to create the arc
        cylinder(h=h+2*e,r=radius-t/2);
        //cubes used to remove materials in order to have only one quarter of an arc
        translate([d,d,0])
        cube([rad_arc+t,rad_arc+t,h+2*e]);
        translate([-rad_arc-t,0,0])
        cube([rad_arc+t,rad_arc+t,h+2*e]);
        translate([-rad_arc-t,0,0])
        cube([rad_arc+2*t,rad_arc+2*t,h+2*e]);
        translate([-rad_arc-t-d,-rad_arc-t,0])
        cube([rad_arc+t,rad_arc+2*t,h+2*e]);
      }
  }
}

translate([-rad_arc,0,0])
arc(thick,h,rad_arc);

```
A part that encoutered some bugs was the difference between the cylinder and the cubes that are used to remove material. It is essential to allow to have only a quarter of an arc, but its parametrization was tricky. It is more detailed in the following section *Parts combinations*.   


#### Parts combinations

To finish the flexilink, you can simply call the previously created functions as indicated in the code below. Note that you'll need to rotate and translate some part so that they are correctly placed. Ensure that these operations are also parametrically coded.

```
// File : mod2_flexilink001.scad
// Author : Morgan Tonglet
// Date : 26 février 2023
// License : Creative Commons Attribution-ShareAlike 4.0 International [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)

union(){
    translate([0,rad_ext,0])//not rad_in
    part(n_holes,h,rad_in,rad_ext,holes_gap);
    translate([-rad_ext-rad_arc,-rad_arc,0])
    rotate([0,0,90])
    part(n_holes,h,rad_in,rad_ext,holes_gap);
    translate([-rad_arc,0,0])
    arc(thick,h,rad_arc);
}
```

Even if this operation seems easy, a problem happened and is shown on the image below. A small space insert itself between the arc and the basic parts, as shown on the picture left-below . To correct this, parameters used to create the arc (and more specifically to remove some parts of the cylinders) had to be modified and better selected in order to allow the arc to be a little bit longer, as shown on the picture right-below. Another parameter `d` has also been introduced to better manage it. As you can see, the gap allowed is still a little bit too long, so it still had to be a little bit shorten. The code presented in the previous **Arc** section already takes these changes into account. Note that the picture at the bottom-left has a small **fn** parameter (about 8) in order to decrease the number of faces generated for cylinders in order to better highlight the space inserted.  

![](images/mod2/mod2_problems.png "Problems")


#### Tests

To test the good functioning of the code, each input parameter was modified. It allows us to check that the code behave correctly when changing a specific parameter and that there are no bug. Values for each parameters are presented in the table below, and the rendering of the total piece is shown in the picture below this table. You can observe in particular that the radius of the arc, the size and the number of holes has been modified, and that it behaves as wanted. The whole code is available [here](https://gitlab.com/fablab-ulb/enseignements/2022-2023/fabzero-experiments/students/morgan.tonglet/-/blob/6879163f6642140eeff1e736ede670712a242983/docs/fabzero-modules/files/module2/mod2_flexilink001.scad).      

| N° | h | rad_in | rad_ext | rad_arc | n_holes | holes_gap | thick |
| ----------- | ----------- | ----------- | ----------- | ----------- | ----------- | ----------- | ----------- |
| top-left   | 5 | 4 | 5 | 50 | 3 | 3 | 0.5 |
| top-center | 5 | 4 | 5 | 50 | 4 | 3 | 0.5 |
| top-right  | 5 | 4 | 5 | 100 | 4 | 3 | 0.5 |
| bot-left   | 5 | 4 | 5 | 50 | 3 | 3 | 0.5 |
| bot-center | 5 | 4 | 5 | 80 | 3 | 6 | 2 |
| bot-right  | 5 | 4 | 2 | 40 | 3 | 6 | 2 |

![](images/mod2/mod2_tests.png "Tests")


## 3. Licensing the work

Licensing your work is an important step when working  in open source. Here we use the *Creative Commons License* that allows some people to reuse the code to a certain extent and within certain rules. The license chosen is [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/) (Creative Commons Attribution-ShareAlike 4.0 International) which allows some people to share and adapt the code while giving credit to the author and share an eventual adaptation or remixing of the work under the same licensing terms. An example of how to license your work is given below :

```
// File : mod2_flexilink001.scad

// Author : Morgan Tonglet

// Date : 26 février 2023

// License : Creative Commons Attribution-ShareAlike 4.0 International [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)
```

## 4. Reuse and adapt another code

In this part, I have used and modified the code of another student while licensing it correctly, following what was explained just above.

### Modifying

The original piece of Antoine Trillet, whose code is avalaible [here](), is shown here below on the left of the picture. The modified piece is shown on the right of this same picture.

![](images/mod2/mod2_atrillet_original_and_modified.png "")

To achieve this second piece, large part of the code were simply deleted. An additional parameter `dist_pins` has been introduced to control the space between the two pins. It was not present in the original work since there was only one pin. The function `module AttachPins(){...}` has not been modified, while the function `module Support(){...}` has been simplified in order to keep only a simpe rectangle.

```
$fn = 100;

//!!!!everything in cm when using factor of 10!!!!
sizeFactor = 10; 

//the thick handle (length, witdth, thickness)
hand = sizeFactor * [8, 3, 1.1];
joint = sizeFactor * 0.8;        //thickness of hand at the joint will influence thickness of handSupp
edgeRounding = sizeFactor * 0.2;

//Pins
PinThickness = sizeFactor * 0.1; 
PinH = joint + sizeFactor * 0.15;
PinOut = sizeFactor * 0.95;
PinIn = PinOut -PinThickness;
ConeOut = PinOut + sizeFactor * 0.15;
ConeH = sizeFactor * 0.15;
PinSpacer = sizeFactor * 0.5;
NbOfPin = 4;

dist_pins = sizeFactor * 3;

module AttachPins(){
    difference(){
        union(){
            cylinder(h = PinH, r = PinOut);
            translate([0,0,PinH])
            cylinder(ConeH,ConeOut, PinIn);
            translate([0,0,PinH - ConeH])
            cylinder(ConeH, PinIn, ConeOut);
            
        }
        minkowski(){
            translate([0,0, edgeRounding - 0.01])
            cylinder(2*PinH,r = PinIn-edgeRounding);
            sphere(edgeRounding);
        }
        for(i = [0 : NbOfPin-1]){
            rotate([0,0,360/NbOfPin*i])
            translate([0,-PinSpacer/2, -0.1 *  PinH]) 
            cube([2*ConeOut,PinSpacer, 2*PinH ]); 
        }   
    }
}


module Support(){
    union(){
      minkowski()
      {
        cube([hand[0], hand[1],hand[2]]-[edgeRounding, edgeRounding, edgeRounding]);
        
        sphere(edgeRounding/2);
      }
    }             
}

module FullSupport(){
    union(){
        translate( [-hand[0]/2,-hand[1]/2,edgeRounding/2])
        Support();
        translate( [dist_pins/2,0,hand[2]])
        AttachPins();
        translate( [-dist_pins/2,0,hand[2]])
        AttachPins();
    }
}


FullSupport();

```

### Licensing

Since I have reused part of the code of someone else, it is important to cite the original work. It is done as indicated below, by adding a field **Credits** to the license part. This text must be added at the top of the code/file. Since the original license was **CC BY-SA**, I must share my adaption of the work under the same license (**SA** = ShareAlike) and by giving credits to the original work (**BY**). Other terms of sharing under the Creative Commons License exist and can be found [here](https://fr.wikipedia.org/wiki/Licence_Creative_Commons).

```
// File : atrillet_pins.scad

// Author : Morgan Tonglet

// Date : 8 march 2023

// License : Creative Commons Attribution-ShareAlike 4.0 International [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)

// Credits :  The original work is attributed to Antoine Trillet, shared under the CC BY-SA license. The original version of this file can be found [here](https://gitlab.com/fablab-ulb/enseignements/2022-2023/fabzero-experiments/students/antoine.trillet/-/blob/main/docs/fabzero-modules/files/module2/openSCAD/handlePart2.scad)

```

## 5. Appendices

### Resize with GraphicsMagick

In order to generate some of the figures of this module, the *GraphicsMagick* library has been used, and more specifically the `gm convert -resize` function that allows to resize an image to a given resolution. For instance, you can use the following command to resize an **input.png** image to an **output.png** image with an height of 300 pixels :  ` gm convert -resize x300 input.png  output.png`.
