#  Group dynamics and final project

This week I worked on defining my final project idea and started to getting used to the documentation process.

## 1. Project Analysis and Design - 2/3/2023

Due to the *JobFair Engineers*, I was not able to assist to this class. However, even if I didn't documented this part that was about a theorical exercise (making problems and objectives tree), I've done the assignment for the final project of my group. You can read it at the end of this page, in section 5.4. 

## 2. Group formation and brainstorming on project problems - 7/3/2023

The objective of this work session was to brainstorm about a project problem. We were asked to bring an object that represents topics or issues that are important to us.

### My object 

The object I brought was an *Ecocup*, because it reminds me ecology and sustainability, but also make me think about the massive plastic usage currently present in our societies, which has a strong and negative impact on the nature. Additionnaly, and to a lesser extent, it makes me also think about the alcohol consumption and the damage it can do to health. I would like to work on one of those problematics, and provide a sustainable solution to a modern problem.

### Brainstorming in group

Here I resume the **objects** that were brought by the members of my group and why they brought it, what it represents for them.

- a **bowl** to represent famine
- an **ecocup**, mine, for ecology and plastic consumption
- a picture of a **pigeon heart** that represents health
- a **dice** for addiction

On a big sheet of paper, we wrote each of the thematic we care about (i.e. what our object represents) and we started to make links and to deduce new subjects from it. The picture below resumes our thinking work, with some of the topics we have identified and which we could be worked on. The identified topic that pleased to everyone is *Health*.

![](images/mod5/brainstorm_scheme.png)

### Problematic identification

To help us brainstorming and identifying some problematics, we have used a specific social tool. First, one person had to say "I would like to..." followed by a subject/problematic he would like to work on. Then, each member of the group in turn, we had to say "Instead of you, I would..." followed by another idea. During this process, you should not hesitate to say things that might not make sense because, paradoxically, this can lead to innovative topics/problems. It opens your mind which is the main objective here.  

During the process, we have also send a representative to other groups in oder to open our minds and enlarge the field of possible problematics. Similarly, we have accepted other's representative to help them back by coming towards them with potential new ideas.

Identifying problematics was more difficult than I would have thought. During the process, we often came with direct solutions instead of identifying a real problematic that we can then solve. The picture below resumes this brainstorming process about problems identification. Subjects that are indeed solvable problematics are underlined in orange. You can observe that the half that came out of this process are not problems, but rather general subjects or already made solutions. 

![](images/mod5/brainstorm_list.png)

## 3. Group dynamics - 28/3/2023

Here I detail some methods of group dynamics that could be used for our group project and that, I hope, will help to the functioning of the group.

### Structure of a meeting

- Entry weather
- Roles
- Agenda/Order of the meeting
- ...
- Planning - eventually roles for next meeting
- Task repartition
- Exit weather

At the beginning of each session, the group do a round table discussion about how each person feel. It is not a therapy, we just "name the things". For instance, it is important to say if you are tired. If everyone is, it could be relevant to report the eventual big decisions that the group could take. That is the entry weather.

Then, you define roles. The main roles are secretary, facilitator and time manager. You can of course add roles that you think are relevant. Don't confuse it with project roles, which are rather in relation to the tasks assigned to each person, not linked to the role of the meeting. 

Then, we can finally talk about serious things related to the project. It begins with the agenda of the meeting that resume the main points that the group must go through. This agenda can be prepared in advance, or written at this moment. The important thing is to go through it collectively to be sure nothing is missing.

After that, we can finally get to the heart of the matter and talk about each item on the agenda in details. At the end, the whole group decides on a schedule, the remaining tasks and their distribution.

### Tips

To define roles of each person for a meeting, each member of the group score between 0 and 5 its desire to do a specific role. It is done at the same time with the finger of the hand. The person that scores the maximum will take the role.

### Decision methods

In a group project, there are often strategic moments when important decisions have to be made. We therefore need to define in advance how the group will take these decisions. Below are a few examples of community decision-making processes.

- **Consensus** : everyone agree with an option that has been eventually discussed.  
- **Random draw** : not the best, make a random choice from a range of possibilities. See [https://plouf-plouf.fr/](https://plouf-plouf.fr/).  
- **Votes** : it can be weighted. For instance, each member of the group has 3 votes that each should allocate through 5 different options. The option with the highest total score wins. You can of course keep a normal vote with only one vote.   
- **Majority decision** : 1 vote, the majority win. Several possible voting rounds.  
- **Consent/agreement** : I prefer another option, but I agree with another option even if it is not my prefered one. It is a decision without objection!  
- **Temperature check** : you check the temperature of each person with the hand. Raising your hand high enough means that you agree with it. On the other hand, keeping it quite low means that you don't agree. You can modulate your desire by changing the height of your hand.   

### Feedback

Collective feedback (about the group functioning) and individual feedback (each person gives its feedback of the group).

Non violent communication:
- **I** : talk about myself.
- **Facts** : talk about concrete facts.
- **Emotions** : talk about you feeling .
- **Requests** : provide an eventual solution.

**Example :** About delay, **I** think that the **regular delay** of this member is a problem. It **bothers** me since it is already logistically complicate personnaly to come at time. I **propose** that we either delay each meeting of 30 minutes or everyone make an effort to come at time. 

### Establishment of the future group functioning

**We are Group 6.**

In general, for our group, we have decised to prefer consensus. Since we are only four in the group, we think it is easily achievable. If there are strong disagreements, we will operate votes. If there are many options, we will consider weighted votes. 

We have decided to separate the decision methods between strategic and operationnal decisions. Strategic decision will be decided first through consensus, then majority judgement, then vote (eventually weighted) if the first two options don't work. For operationnal decision, we will vote or draw at random if there is a tie, which will (we hope) help us gaining time. For the determination of meeting roles, we have tried the scoring between 0 and 5 with the hand today. For future meetings, we will prefer making a turn between roles and rely on the goodwill of each members. If needed, we can always come back to the first method. 

About communication, we will use Messenger. We have also shared our telephone numbers.

**Individual feedbacks:**
- **Serge** : He hopes everyone in the group are good workers. He also hopes that Léon that has an impediment today will be there next times.
- **Morgan** : I am not afraid that we will achieve good work together and that the final project will be done well. Léon seems motivated. I know better Julien and I know that he is a good worker. 
- **Julien** : More or less the same.
- **Léon** : /

We have not really been able to provide a collective feedback since the project has not really beginned. 

## 4. Problematic identification & analysis - 29/3/2023

This section is as a continuation of the work that was done at the session on *5.2. Group formation and brainstorming on project problems* on 7 March 2023. As a recall, the subject our group would like to work on is "Health".

### An inverted pyramid...

Starting from the "health" subject, we have tried to reach a question, such as *"Is it possible to improve the precision of this device ?"* (it is just an example). This question should represent a problematic for which a solution is possible and achievable by 4 students in a FabLab. 

To reach this question/problematic starting from the subject, we have first defined a domain, an area of work, here "health". We have restricted ourselves to a therapeutic, meaning developping something that improves the the well-being of a patient. Then, we have still narrowed ourselve to a specific system, here the physiological/physical system, meaning a problemn that we could solve with a mecanic device (e.g. a broken arm). We focused ourselves on trying to provide a solution to the symptoms of a disease instead of on the causes of a certain disease, and more specifically to elders.

An non-exhaustive example of a path of thought that doesn't represent our thought process is indicated in the picture below (the so-called pyramid).


![](images/mod5/tree_001.jpg)

We have tried to reach a problematic, but the field of possibilities was so large that we didn't reached any problematic. On the advice of an expert in the field, we imposed a strong and arbitrary constraint on ourselves to try to bring out a problematic. This constraint was working on the arm, and even more specifically on "**the hand**".

Some problematics then emerged, especiallly linked to elders, and more particularly in terms of :
- loss of strength,
- joint pain,
- coordination problems,
- hand shaking,
- ...

Another problematic that has emerged in the arm field was about "plaster cast that stink", but it has not been investigated. Our thought process is illustrated on the picture below.


![](images/mod5/tree_002.jpg)


We have decided to focus on **hand shaking** which can be caused by Parkinson's disease for instance.

### ... and some trees

At this step of the process, we have drawn a problem tree that is showed here below. The trunk represents the chosen problematic, while roots and leaves respectively represent the causes and consequences linked to this problematic.

![](images/mod5/tree_004.jpg)

After that, we turned this into an objective tree that is showed below. The trunk represents objectives, roots represent activities that could solve the problematic and leaves represent their impacts.


![](images/mod5/tree_005.jpg)


It is not indicated on the picture just above but the final solution that has been kept is related . Next steps of the process of the group are available on the website of the group [here](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/groups/group-06/).