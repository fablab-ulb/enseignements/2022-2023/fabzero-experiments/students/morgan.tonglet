# Project management and documentation

This week I started to getting used to the documentation process. I also linked my computer to the Gitlab server using a SSH key in order to establish a secure connection between my PC and the distant server of Gitlab. I made myself familiar with markdown by writing this documentation.

## 1. Prerequisites

To be able to work on a group project and use a versioning tool, you will need a GitLab account. You can create one [here](https://gitlab.com/) on the official website.

## 2. Git and bash prompt installations

In order to allow to have a local version of a project on your computer and to work in team, you need to install a bash terminal and git. Git was already installed on my computer, and so was the terminal. Note that the default command prompt in Windows (which is my operating system) does not work on old version. Now, with more recent version of Windows, you can just type `bash` in command line to run any bash command. To install git, you can follow [this tutorial](http://codeur-pro.fr/comment-installer-git-sur-windows/).

## 3. Link your Gitlab account with a SSH key 

This part was done thanks to the nice tutorial of Pauline Mackelbert available [here](https://fablab-ulb.gitlab.io/enseignements/2021-2022/fabzero-experiments/students/pauline.mackelbert/FabZero-Modules/module01/?fbclid=IwAR3K50eo1yNPQx49ED1TBqv5JORd3loht-x9jzw4chiAaNitzeZK4Lm0PQg). If you encounter difficulties, don't hesitate to check it !

### Why SSH keys ?

There are multiple possibilities to work together on a project through git. You could use HTTPS links to clone your project each time you work on it. You'll however need to connect you at each work session by specifying your password and your username, which can fast become boring. Using .ZIP files is also a possibility, but it is very time consuming since you'll need to compress and unzip each time a quite big folder. The option that has been privilegied is the SSH key. There are multiple steps that could appear complicate but, once done, a secure connection is definitely defined between your computer and the distant repository, and you won't (normally) need to perform some boring steps again for a long time when cloning, pushing or pulling, and that for any repository.

### Setup steps

First, open a terminal and go into you *C:/Users/username/.ssh/* directory, where *username* has to be replaced by your actual user name. You can do it directly by typping the following command in terminal the `cd "C:/Users/username/.ssh/"`.

The next step is to generate a pair of public and private keys. Type `ssh-keygen -t ed25519 -C "mail@ulb.be"` in the terminal and press enter. The `-C "mail@ulb.be"` option adds a comment at the end of the key. You will need to complete some fields asked in the terminal, as indicated in the screenshot below. 

![](images/mod1/mod1_screen001.png "SSH key generation")

In the field asking the name of *the file in which to save the key*, let it empty by pressing enter. In the field asking the passphrase, enter a personal password of your choice. Choose it well because you'll need it later, even possibly several weeks or months after you created it.

Then, copy the public key into the clip board by typing the `cat ~/.ssh/ide_ed25519.pub | clip` command. Add it to your Gitlab account by clicking on the logo at the top right of the ***Home*** page of your Gitlab, and then navigate to the ***Edit profile > SSH Keys*** section. Paste the public key in the ***Key*** field by pressing simultaneously the `Ctrl+V` keyboard keys. In the ***Title*** field, write the something like *My Dell laptop* allowing you to recognize the key.

To make sure everything went well, verify the connexion by typing the `ssh -T git@gitlab.com`, and confirming the operation. 

### Problems encountered

A first attempt has been done by generating the keys pair in another directory in the *Documents* folder, which led to some bugs. Make sure to do all the operations in the *.ssh* folder as specified above when generating the SSH key.  

A second attempt has been conducted but using a different file name that the one indicated above, which also led to bug and an incapacity to connect to the server. Thus, to avoid that, make sure to keep the default name *id_ed25519* when running the `ssh-keygen` command by keeping the first field empty.

The output obtained is shown in the picture below.

![](images/mod1/mod1_screen002.png "Bug with SSH")

## 4. Git clonage

First, go in the directory where you want to put your project folder. Don't create one, it will be done automatically by getting the remote version of it. A good exercise so far to get used to the command line interface (CLI) is to combine the `ls` command that shows you the file present in the current directory, and the `cd` command to move through the arborescence of your disk. Note that the command `cd ..` allows you to move up/back in the arborescence, while the `TAB` keyboard key will complete a file or directory name when using the `cd` command, which avoid you to specify entire filenames each time.

When you are in the willen repository, type the `git clone gitlab@link` where the link is available in the dashboard, in the specific project you want to clone. It the dashboard is empty, create a new project and copy its link.
Congrats, you have now the remote project on your computer !

## 5. Version control

When you'll need to work on your project, there are two main operations that you'll do : the **pull** and the **push** operations. A third command that can be useful is `git status` that gives you a summary of the status of files and version control.

### Pull operation

It is the simpliest one, and it consists in getting the remote version of you work (the one that is one the distant server) and put it on your local computer. To do so, open a prompt and place yourself at the place of your project. Then, just type `git pull`.

### Push operation 

This operation is a little bit more complex. Indeed, when you create new files on your local computer, you first need to add it to the version control. To add by default every file present in the repo, type `git add *` and press enter. However, if you just want to add specific files to the version control, type `git add path/file_name`. 

To send the last modifications of every file to the version control, type `git commit -m "a message"` where the message must be clear about the modifications you've added. Finally, type `git push` and press enter so that modifications are sent to the distant server.

## 6. Report

This report has been writed using *Markdown*. Pictures are results of screenshot that I have then processed using the *GraphicsMagick* library in command line.

### Markdown

Its functioning is quite simple, and you can find all the essentials by clicking here. Here I make a quick overview of basic usage: 

- To write a title, use the **# Title** syntax. Subtitles are written **## Subtitle**. You can add as many # as you want to make sub-sub-sub-...-title if you want. 

- To write in bold, use double asterisks **\*\*text\*\***. To write in italics, use simple asterisk such as *\*text\**. To write short code snippets into a text, use grave accent. Writing **\`code\`** will produce `code`.

- To insert an image, use the syntax **\!\[\]\(path "caption")**. For example, the code for the first image is **\!\[\]\(images/mod1_screen001.png "SSH key generation"\)**.

- To insert an external link to a website for instance, use the **\[here\] \(wikipedia.org)** syntax to produce a clickable [here](wikipedia.org) that will redirect you to the *Wikipedia* website.


### GraphicsMagick

The *GraphicsMagick* is a library useful when you want to fast process some change to an image, such as a resize, or sticking several images together. To install the library, run the following command in a bash prompt : `sudo apt install graphicsmagick`. You will maybe need to specify your administrator password. 

For this first module, the library was only used to resize image so it is lighter when uploading it to the server. For instance, screenshots taken on my computer have default size of 1920x1080 pixels. To reduce it, I open a bash prompt, and I go to the directory where the picture **example.png** stands. It is the image that I want to resize. To do so, and reducing the size by half, run the `gm convert -resize 960x example.png example_BIS.png` command. It will create a second image of size 960x540 pixels in the same directory. If you just want to replace the original file, just write twice the name of the original file instead of **example_BIS.png**  
