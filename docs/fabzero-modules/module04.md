#  Selected fablab tool - Electronic prototyping

This module is about my work on electronic prototyping. I got thus familiar with Arduino which is the tool used for this purpose. The microcontroller I used here is the *Generic RP2040* board which come with the *Raspberry Pi Pico* serie of boards. A scheme of the used board is showed just here below.

![](images/mod4/arduino_scheme_001.jpg "step 3")

## 1. Set-up steps

1) Download the Arduino sofware [here](https://www.arduino.cc/en/software). Choose the version compatible with your operating system.

2) Launch the .exe file you've just downloaded. Follow the instructions and launch the application. Keep the default parameters.

3) In the Arduino interface, go to *File > Preferences*. In the *Additional Boards Managers URLs*, copy-paste this link: [https://github.com/earlephilhower/arduino-pico/releases/download/global/package_rp2040_index.json](https://github.com/earlephilhower/arduino-pico/releases/download/global/package_rp2040_index.json) and confirm the operation.

![](images/mod4/screen_001.png "step 3")

4) Follow the steps showed on the image below. It will install the correct dependencies to recognise the board. First, go to the *Board manager* either in the left panel or in the *Tools > Board: > Boards manager...* section. Then, filter by typing *pico* in the searching bar. Finally, install the *Raspberry Pi Pico/RP2040* dependence by *Earle F. Philhower, III*.

![](images/mod4/screen_002.png "step 3")

4) Plug in the USB cable connected to your Arduino while maintaining the *Boot* button pressed. This is important to do it like that whenever you want to upload a new program to the board.

![](images/mod4/arduino_001.jpg "step 4")


5) Go to *Select Board > Select other board and port* near the toolbar. In the *Board* field, select the *Generic RP2040* board. In the port field, select the *UF2* port.

You are now connected to your µ-controller ! The next step is to write some code and upload it to the *Arduino*.

## 2. First program

The code below is one of the simpliest you can write on an Arduino. It just blinks a led on the board. The code in the `loop()` function activate a led and, after 500ms, deactivate this same led represented by `ledpin`. `ledpin` has been set to 25 because this is the pin indicated in the [datasheet](https://fablab-ulb.gitlab.io/enseignements/fabzero/electronics/) for the led under the name of *LED (GP25)*. 

``` 
int ledpin = 25; //see in datasheet of pico RP2040

void setup() {
  // put your setup code here, to run once:
  pinMode(ledpin, OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  digitalWrite(ledpin, HIGH);
  delay(500);
  digitalWrite(ledpin,LOW);
  delay(500);
}
```

This code is inspired from an example provided with the Arduino software. Other examples exist and are available in the *File > Examples* section of the software, as indicated in the image below.

![](images/mod4/screen_003.png "get examples")


## 3. RGBW led

The Raspberry Pi Pico/RP2040 comes with a colored led. However, to be able to talk with it, you'll need to install an additional library. 

1) Go to this [link](https://github.com/adafruit/Adafruit_NeoPixel) and download the code as ZIP file.

2) As indicated in the image below, go to the *Sketch > Include library > Add .ZIP library...* section, and select the ZIP file you've just downloaded.

![](images/mod4/screen_004.png "step 2")

3) Test it. To do so, take one of the examples available in the section *File > Examples > Adafruit NeoPixel* (at the bottom of the bottom). Select one of the available file, for instance the *buttoncycler* code, as indicated in the image below. The whole code is available [here](). 

![](images/mod4/screen_005.png "step 3")

4) Upload the code to the board as done previously, i.e.:
  - First modify the value of the pins by checking the [datasheet](https://fablab-ulb.gitlab.io/enseignements/fabzero/electronics/hardware/specifications/). BUTTON_PIN and PIXEL_PIN should be respectively equal to 24 and 23 according to it.
  - Press the *boot* button and insert the USB cable connected to your µ-controller. 
  - Select the board and the port in the software.
  - Upload it to your Arduino by clicking on the button with the arrow at the top panel.

Normally, it should not do anything special. But, if you press the *USR* button, it should switch on a light. Try to push it several times and you will see the different states that the led can take. 

## 4. DHT20 I2C humidity & temperature sensor

This part is a little more trickier since we will have to use a breadboard, cable, and a external component to build a circuit. The first step is to download the library as a ZIP file from [this link](https://github.com/RobTillaart/DHT20). Then, add it to the *Arduino* software as indicated at steps 1 and 2 of the section 4.3. You can also directly install it from the *Arduino software* through the *Library manager*, just search it using the *DHT20* keyword.

The component studied here is a humidity & temperature sensor called *DHT20*. Its datasheet is available [here](https://cdn.sparkfun.com/assets/8/a/1/5/0/DHT20.pdf). The objective is to measure the temperature thanks to this sensor, and to plot a curve showing its evolution through time. For this purpose, I have directy used a piece of code provided with the library. To do the same as me, go in the *File > Examples > DHT20* panel and select the *DHT20_plotter* file.

Note that a line has to be replaced in the template file. 

Instead of...

```   
DHT.begin();
```

Write...

```   
Wire.setSDA(0);
Wire.setSCL(1);
Wire.begin();

```
... and make sure that the *SDA* and *SCL* ports of the *DHT20* component are respectively connected to *GP0* and *GP1*.

Build the circuit accordingly, as it is shown in the picture below.

![](images/mod4/dht20_circuit_good.jpg "DHT20 circuit ")

Then upload the code to the board, as already explained before.

### Problem encountered

A problem always happened during this process. Indeed, when uploading the code, Windows raised an error telling that the last USB entity connected was not recognised whatever I tried to do. I let the problem away several days and came back after, rebuilding the entire circuit. The code was unchanged (excepting the pin numbers), so the problem certainly came either from the pin numbers or from some cables that were inverted. From this point, the code upload itself well on the board.

But other problems have arisen...

**NOTE:** To be sure that the code uploaded and executed itself well, I have added some lines that made the led blink. The code is available at the end of this section.

The code upload itself well since the led was blinking at regular intervals. However, I was unable to plot the curve that shows the temperature. The origin of the problem is clearly due to the communication that doesn't happen between the DHT20 component and my computer since the code uploaded itself well on the board.   

In an attempt to solve the problem, I have tried to change the communication port as showed in the image below (each one, yes).

![](images/mod4/screen_006_ports.png "step 3")

But nothing changed. I have searched and gathered information on the internet about how to use the *DHT20* componend using *Arduino*, in particular in [this](https://www.youtube.com/watch?v=uwh_mvIw4Ek) and [this](https://www.youtube.com/watch?v=lrDl8NhMwQw) videos, but without success.

I have also read the documentation of the *DHT20* library that is available [here](https://github.com/RobTillaart/DHT20) and tried some modifications that are recommended in the *README*. It didn't change anything.

From what I understood, a graph should normally appear when clicking on the *Serial plotter* button in the Arduino interface, if everything was going fine. But this graph never appeared...

Since I have been stuck on this problem for 10 days, and even the teacher's assistant couldn't help me, I have decided to put this problem aside. I let you the code here below, which includes the small modifications that causes the LED to blink. Don't hesitate to contact me at [Morgan.Tonglet@ulb.be](Morgan.Tonglet@ulb.be) if you find the solution to my problem.

```
//    FILE: DHT20_plotter.ino
//  AUTHOR: Rob Tillaart
//  MODIFS: Morgan Tonglet
// PURPOSE: Demo for DHT20 I2C humidity & temperature sensor
//
//  Always check datasheet - front view
//
//          +--------------+
//  VDD ----| 1            |
//  SDA ----| 2    DHT20   |
//  GND ----| 3            |
//  SCL ----| 4            |
//          +--------------+

#include "DHT20.h"

DHT20 DHT(&Wire);

int ledpin = 25;

void setup()
{
  Wire.setSDA(0);
  Wire.setSCL(1);
  Wire.begin();  //  ESP32 default pins 21 22
  
  Serial.begin(115200);
  Serial.println("Humidity, Temperature");

  pinMode(ledpin, OUTPUT);
}

void loop()
{
  if (millis() - DHT.lastRead() >= 1000)
  {
    digitalWrite(ledpin, HIGH);
    // note no error checking
    DHT.read();
    Serial.print(DHT.getHumidity(), 1);
    Serial.print(", ");
    Serial.println(DHT.getTemperature(), 1);
  }
  else
  {
    delay(100);
    digitalWrite(ledpin,LOW);

  }
}
// -- END OF FILE --
```

## 5. Laser sensor

As an advanced sensor, I have chosen a laser distance sensor. I have not developed any code for this part because I am actually overworked with my Master thesis and because I think that working on this particular won't really help my team to accomplish its final project (this second part of the course has already begun at the moment I write those lines). However, for interested readers, the datasheet of this sensor is available [here](https://www.velleman.eu/downloads/29/vma337_a4v01.pdf).




