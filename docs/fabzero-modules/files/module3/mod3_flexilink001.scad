// File : mod2_flexilink001.scad
// Author : Morgan Tonglet
// Date : 26 février 2023
// License : Creative Commons Attribution-ShareAlike 4.0 International [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)

//Constants
$fn=64;
e=0.05;
sf=1; //size factor 

//Parameters //for LEGO
h=2.0*sf;//2.0
rad_in=5.0/2*sf;//4.8
rad_ext=7.8/2*sf;//7.8
rad_arc=40.0*sf;//80.0
n_holes=3;//3
holes_gap=0.2*sf;//0.2
thick=1.5*sf;//1.5

//Functions    
module part(n_holes,h,r_in,r_ext,gap){
  difference(){
    //drawing the external part
    hull(){
        cylinder(h,r=r_ext);
        translate([0,(n_holes-1)*(2*r_ext+gap),0])
        cylinder(h,r=r_ext);
    };
    //removing material for holes
    union(){
      for(i=[0:n_holes-1]){
        translate([0,i*(2*r_ext+gap),0])
        cylinder(h,r=r_in);
      }
    }
  }
}

module arc(thickness, h, radius){
  t=thickness;
  d=(rad_ext-rad_in)/2;
  difference(){
    cylinder(h=h,r=radius+t/2);
      translate([0,0,-e])
      union(){
        cylinder(h=h+2*e,r=radius-t/2);
        translate([d,d,0])
        cube([rad_arc+t,rad_arc+t,h+2*e]);
        translate([-rad_arc-t,0,0])
        cube([rad_arc+t,rad_arc+t,h+2*e]);
        translate([-rad_arc-t,0,0])
        cube([rad_arc+2*t,rad_arc+2*t,h+2*e]);
        translate([-rad_arc-t-d,-rad_arc-t,0])
        cube([rad_arc+t,rad_arc+2*t,h+2*e]);
      }
  }
}

union(){
    translate([0,rad_ext,0])//not rad_in
    part(n_holes,h,rad_in,rad_ext,holes_gap);
    translate([-rad_ext-rad_arc,-rad_arc,0])
    rotate([0,0,90])
    part(n_holes,h,rad_in,rad_ext,holes_gap);
    translate([-rad_arc,0,0])
    arc(thick,h,rad_arc);
}