//       FILE: DHT20_plotter.ino
//     AUTHOR: Rob Tillaart
//MODIFIED BY: Morgan Tonglet
//    PURPOSE: Demo for DHT20 I2C humidity & temperature sensor
//
//  Always check datasheet - front view
//
//          +--------------+
//  VDD ----| 1            |
//  SDA ----| 2    DHT20   |
//  GND ----| 3            |
//  SCL ----| 4            |
//          +--------------+

#include "DHT20.h"

DHT20 DHT(&Wire);

int ledpin = 25;

void setup()
{
  Wire.setSDA(0);
  Wire.setSCL(1);
  Wire.begin();  //  ESP32 default pins 21 22
  
  Serial.begin(115200);
  Serial.println("Humidity, Temperature");

  pinMode(ledpin, OUTPUT);
}

void loop()
{
  if (millis() - DHT.lastRead() >= 1000)
  {
    digitalWrite(ledpin, HIGH);
    // note no error checking
    DHT.read();
    Serial.print(DHT.getHumidity(), 1);
    Serial.print(", ");
    Serial.println(DHT.getTemperature(), 1);
  }
  else
  {
    delay(100);
    digitalWrite(ledpin,LOW);

  }
}
// -- END OF FILE --
