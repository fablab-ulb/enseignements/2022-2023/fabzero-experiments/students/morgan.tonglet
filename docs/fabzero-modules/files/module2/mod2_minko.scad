// File : mod2_minko.scad
// Author : Morgan Tonglet
// Date : 26 février 2023
// License : Creative Commons Attribution-ShareAlike 4.0 International [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)

//Global parameter
$fn=50; //face number (e.g. 50 to have high curve resolution)

//Piece generation
minkowski(){
    side=10;
    sphere(side/4);
    cube([side*2,side,side/4]);
}