// File : mod2_ecrou.scad
// Author : Morgan Tonglet
// Date : 26 février 2023
// License : Creative Commons Attribution-ShareAlike 4.0 International [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)

//Constant
e=0.01;

//Parameter
fn=8;
radius=8;

//Functions
module ecrou(extR, intR, h, fn){
    difference(){
        cylinder(h=h,r=extR,center=true, $fn=fn);
        cylinder(h=h+e,r=intR,center=true, $fn=fn);
    }
}

//Render
ecrou(radius, radius/2, radius/2, fn);