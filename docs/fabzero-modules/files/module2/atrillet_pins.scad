// File : atrillet_pins.scad

// Author : Morgan Tonglet

// Date : 8 march 2023

// License : Creative Commons Attribution-ShareAlike 4.0 International [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)

// Credits :  The original work is attributed to Antoine Trillet, shared under the CC BY-SA license. The original version of this file can be found [here](https://gitlab.com/fablab-ulb/enseignements/2022-2023/fabzero-experiments/students/antoine.trillet/-/blob/main/docs/fabzero-modules/files/module2/openSCAD/handlePart2.scad)


$fn = 100;

//!!!!everything in cm when using factor of 10!!!!
sizeFactor = 10; 


//the thick handle (length, witdth, thickness)
hand = sizeFactor * [8, 3, 1.1];
joint = sizeFactor * 0.8;        //thickness of hand at the joint will influence thickness of handSupp
edgeRounding = sizeFactor * 0.2;


//Pins
PinThickness = sizeFactor * 0.1; 
PinH = joint + sizeFactor * 0.15;
PinOut = sizeFactor * 0.95;
PinIn = PinOut -PinThickness;
ConeOut = PinOut + sizeFactor * 0.15;
ConeH = sizeFactor * 0.15;
PinSpacer = sizeFactor * 0.5;
NbOfPin = 4;

dist_pins = sizeFactor * 3;

module AttachPins(){
    difference(){
        union(){
            cylinder(h = PinH, r = PinOut);
            translate([0,0,PinH])
            cylinder(ConeH,ConeOut, PinIn);
            translate([0,0,PinH - ConeH])
            cylinder(ConeH, PinIn, ConeOut);
            
        }
        minkowski(){
            translate([0,0, edgeRounding - 0.01])
            cylinder(2*PinH,r = PinIn-edgeRounding);
            sphere(edgeRounding);
        }
        for(i = [0 : NbOfPin-1]){
            rotate([0,0,360/NbOfPin*i])
            translate([0,-PinSpacer/2, -0.1 *  PinH]) 
            cube([2*ConeOut,PinSpacer, 2*PinH ]); 
        }   
    }
}


module Support(){
    union(){
      minkowski()
      {
        cube([hand[0], hand[1],hand[2]]-[edgeRounding, edgeRounding, edgeRounding]);
        
        sphere(edgeRounding/2);
      }
    }             
}

module FullSupport(){
    union(){
        translate( [-hand[0]/2,-hand[1]/2,edgeRounding/2])
        Support();
        translate( [dist_pins/2,0,hand[2]])
        AttachPins();
        translate( [-dist_pins/2,0,hand[2]])
        AttachPins();
    }
}


FullSupport();
