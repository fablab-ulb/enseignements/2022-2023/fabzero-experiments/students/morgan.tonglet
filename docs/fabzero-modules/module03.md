#  3D printing

This week I worked on 3D printing, starting from the work accomplished previous week for the [2nd module](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/morgan.tonglet/fabzero-modules/module02/).


## 1. Prerequisites

To be able to achieve this part, you'll need to install and get used with *PrusaSlicer* which is an opensource software used to print 3D objects. You can download it at [this link](https://www.prusa3d.com/fr/page/prusaslicer_424/).

## 2. My first 3D-printed object

### Adapting the SCAD code

The initial objective of the flexilink designed during the 2nd module and shown again in the picture below was to attach itself to a *Lego* brick. It was thus necessary to adapt the code so that the proportions of our flexilink match those of a *Lego* brick. The dimension of a such a brick are recalled here below.

![](images/mod3/mod3_flexilink.png "")

![](images/mod3/mod3_Lego_dimensions.png "")

Therefore, each parameter now take the values presented below. The full code is available [here](https://gitlab.com/fablab-ulb/enseignements/2022-2023/fabzero-experiments/students/morgan.tonglet/-/blob/main/docs/fabzero-modules/files/module3/mod3_flexilink001.scad). 

```
//Constants
$fn=64;
e=0.05;
sf=1; //size factor 

//Parameters //for LEGO
h=2.0*sf; 
rad_in=5.0/2*sf;  //4.8 originall
rad_ext=7.8/2*sf; 
rad_arc=40.0*sf;  //80.0
n_holes=3;
holes_gap=0.2*sf;
thick=1.5*sf;
```
Once dimensions are correctly set, generate a **.stl** file that the *PrusaSlicer* software will be able to read. To do so, while you are in *OpenSCAD*, go to *File > Export > Export as STL*.

### Generating the code for the 3D printing machine

Launch *PrusaSlicer*. As indicated in the picture below, go in the *File > Import > Import STL/3MF/...* and select the **.stl** file you have just created.

![](images/mod3/prusa_001.png "")

There are some printing parameters that can be modified and present on the top-right of the picture below, for instance the *Filament* parameter, the *Printer*, etc. Keep the default values. However, you can play with the *Infill* parameter. For this module, we have chosen an *Infill* parameter of 20%. At this stage of the process, you can also move the piece so  it will be printed using another face as a basis. Try to print you piece on the face with the highest possible area. When your parameters are as desired, click on the *Slice now* button at the bottom-right of the interface.

![](images/mod3/prusa_002.png "")

At this stage, you can see and observe how the software has filled in the piece. There is nothing special to do at this point, you can thus click on the button *Export G-code* which will create a file in a language that is understandable by the 3D printer.

![](images/mod3/prusa_003.png "")

Move the file on an external drive (USB-key or SD-card) that your printer can read. Follow instructions and launch the print.

### Results

The result of the print is shown in pictures below. As you can see, the printer is not very precise since we can observe a lot of filaments going outside of the contour, and even filling a little bit the holes. Better results could be obtained by changing the printer.

![](images/mod3/flexilink_append.jpg)

To correct imperfections, especially in holes, I used a sanding file instead of reprinting, which gave the final result below.

![](images/mod3/flexilink_final.jpg)


### Tests & adaptation of the model

To be sure the flexilink is correctly designed, we should test it with *Lego* bricks. If problems arrise, such as holes that are too wide or too tight, or if the flexilink isn't flexible enough, we should reiterate the whole process using new parameters corrected accordingly. This process can be very plastic consuming, so another option could be to print only one piece with holes of different sizes that can be easily identified. Then, keep the size that fit the best. This method is to be preferred since it saves time and plastic.

This test stage has not been conducted since I have no access to *Lego* bricks. Instead, the following section is about another piece that is printed and that should imbricate itself in our flexilink.


## 4. Appendices

### GraphicsMagick

To generate the last picture of this model, two commands of the *GraphicsMagick* library were used. First the original images were compressed by calling the following command with **i** each time the value 1 and 2:  

`gm convert -resize x504 image_i.jpg im_i_comp.jpg`

Then, I called the below command, which concatenate two images horizontally, and which gave the image above:  

`gm convert -append im_1_comp.jpg im_2_comp.jpg final_image.jpg` 
