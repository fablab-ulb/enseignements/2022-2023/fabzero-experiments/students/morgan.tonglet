Hi !

Welcome to my blog ! Here you'll find some information about me and my work. Enjoy :)

## About me

My name is Morgan Tonglet. I am a civil engineering student at the *Université Libre de Bruxelles*. I am currently in my last year of study (Master 2, computer engineering option) and I expect to graduate at the end of this academic year 2022-2023.


## My background

During my secondary school, I liked and was really good in math and science. That's why I oriented myself to civil engineering studies. I discovered information technology and I was really surprised at the possibilities that this discipline offers from almost nothing. You can literally do almost anything with just a PC and some code. This is the reason why I decided to do a master's degree in computer science, and more specifically in artificial intelligence.

My motivation for this course is to learn how to use fablab machines. Up until now, my course has been fairly theoretical, but I prefer practical exercises that involve tinkering with things and finding innovative solutions. So I hope I'll have the opportunity to develop my skills in 3D printing, laser cutting, electronics programming, etc.

## Previous works

During my studies, I had the opportunities of working on various project, and not just about IT. For instance, during my first year at the *École Polytechnique de Bruxelles*, I worked within a team of 6 other students on a project whose objective was to develop a solar pepper dryer as part of a development aid project supervised by Codepo. The good work we achieved allowed my to go to Cambodia for a month to install the final device in the field. You can find more information about this project [here](https://polytech.ulb.be/fr/ecole/actualites/en-direct-du-cambodge-1-decouvertes-et-surprises).
